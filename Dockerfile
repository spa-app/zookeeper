FROM amazoncorretto:11

ARG ZOOKEEPER_MIRROR=https://downloads.apache.org/zookeeper/current/

ENV ZOOKEEPER_VERSION=3.6.0
ENV PATH /opt/zookeeper/bin:$PATH

RUN mkdir -p /opt/zookeeper /etc/zookeeper

RUN echo "===> installing Zookeeper-${ZOOKEEPER_VERSION}..." \
    && yum -q -y update \
    && yum -q -y install tar gzip \
    && curl -s $ZOOKEEPER_MIRROR/apache-zookeeper-${ZOOKEEPER_VERSION}-bin.tar.gz | tar xz -C /opt/zookeeper --strip-components=1 \
    && echo "===> clean up ..."  \
    && yum -q -y remove tar gzip \
    && yum clean all \
    && rm -rf /tmp/* \
    && rm -rf /var/cache/yum

RUN cp /opt/zookeeper/conf/zoo_sample.cfg /etc/zookeeper/zoo.cfg
WORKDIR /opt/zookeeper

ENTRYPOINT ["bin/zkServer.sh"]
CMD ["--config","/etc/zookeeper", "start-foreground"]
